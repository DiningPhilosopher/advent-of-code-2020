//$(which go) run $0 $@; exit $?

package main

import (
  "fmt"
  "bufio"
  "strconv"
  "os"
  "log"
)

func main() {
  file, err := os.Open("input_a.txt")
  if err != nil {
    log.Fatal(err)
  }
  defer func(){
    if err = file.Close(); err != nil {
      log.Fatal(err)
    }
  }()
  scanner := bufio.NewScanner(file)
  var numbers []int
  for scanner.Scan() {
    int_read, _ := strconv.Atoi(scanner.Text())
    numbers = append(numbers, int_read)
  }
  for _, ie := range numbers {
    for _, je := range numbers {
      if ie + je == 2020 {
        fmt.Println(ie * je)
        return
      }
    }
  }
}

