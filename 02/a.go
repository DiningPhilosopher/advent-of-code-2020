package main

import (
  "fmt"
  "bufio"
  "strconv"
  "regexp"
  "log"
  "os"
)

func parse_line(line string) (string, byte, int, int) {
  re := regexp.MustCompile(`([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)`)
  groups := re.FindStringSubmatch(line)
  if groups == nil {
    return "", 0, -1, -1 
  }
  low, _ := strconv.Atoi(groups[1])
  high, _ := strconv.Atoi(groups[2])
  return groups[4], groups[3][0], low, high
}

func is_valid(password string, char byte, low int, high int) bool {
  count := 0
  for i := 0; i < len(password); i++ {
    if password[i] == char {
      count++
      fmt.Println("Match")
    } else {
      fmt.Println("Mismatch")
    }
  }
  fmt.Println(low, high, count)
  if count <= high && count >= low {
    return true
  }
  return false
}

func main() {
  file, err := os.Open("input.txt")
  if err != nil {
    log.Fatal(err)
  }
  defer func(){
    if err = file.Close(); err != nil {
      log.Fatal(err)
    }
  }()
  scanner := bufio.NewScanner(file)
  valid := 0
  for scanner.Scan() {
    line := scanner.Text()
    if len(line) > 0 {
      pass, char, low, high := parse_line(line)
      if is_valid(pass, char, low, high) {
        valid += 1
        fmt.Println("***************** Right!")
      } else {
        fmt.Println("***************** Wrong!")
      }
    }
  }
  fmt.Println(valid)
}
