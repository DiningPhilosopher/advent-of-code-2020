package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	//   "strings"
)

func slide_trees(mountain []string, right int, down int) int {
	trees := 0
	col := 0
	width := len(mountain[0])
	down_skip := 0
	for _, cliff := range mountain {
		if down_skip == 0 {
			if cliff[col] == '#' {
				trees++
			}
			col = (col + right) % width
			down_skip = down
		}
		down_skip -= 1
	}
	return trees
}

func main() {
	file, err := os.Open("input_a")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	var mountain []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		mountain = append(mountain, line)
	}
	angles := [][]int{{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}}
	product := 1
	for _, right_down := range angles {
		var trees = slide_trees(mountain, right_down[0], right_down[1])
		product *= trees
		fmt.Println(trees)
	}
	fmt.Println(product)
}
