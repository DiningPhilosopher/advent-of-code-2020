package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

func passportSplitFun(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := strings.Index(string(data), "\n\n"); i >= 0 {
		return i + 1, data[0:i], nil
	}
	if atEOF {
		return len(data), data, nil
	}
	return
}

func oneLine(passport string) string {
	initNewline := regexp.MustCompile(`^\n`)
	result := initNewline.ReplaceAllString(passport, "")
	allNewlines := regexp.MustCompile(`\n`)
	result = allNewlines.ReplaceAllString(result, " ")
	return result
}

func readPassports(path string) []string {
	var passports []string
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal()
		}
	}()
	scanner := bufio.NewScanner(file)
	scanner.Split(passportSplitFun)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
		passport := oneLine(scanner.Text())
		passports = append(passports, passport)
	}
	return passports
}

func goodPassports(passports []string) int {
	valids := 0
	req := []string{"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
PassportLoop:
	for _, pass := range passports {
		for _, rq := range req {
			re := regexp.MustCompile(rq + ":[a-z0-9#]+")
			requiredMatch := re.Match([]byte(pass))
			if !requiredMatch {
				continue PassportLoop
			}
		}
		valids++
	}
	return valids
}

func main() {
	passes := readPassports("input")
	log.Print("***** Answer *****")
	for _, p := range passes {
		fmt.Printf("Pass: %q\n", p)
	}
	fmt.Printf("%d\n", goodPassports(passes))
	return
}
