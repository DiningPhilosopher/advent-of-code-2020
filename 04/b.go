package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func passportSplitFun(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := strings.Index(string(data), "\n\n"); i >= 0 {
		return i + 1, data[0:i], nil
	}
	if atEOF {
		return len(data), data, nil
	}
	return
}

func oneLine(passport string) string {
	initNewline := regexp.MustCompile(`^\n`)
	result := initNewline.ReplaceAllString(passport, "")
	allNewlines := regexp.MustCompile(`\n`)
	result = allNewlines.ReplaceAllString(result, " ")
	return result
}

func readPassports(path string) []string {
	var passports []string
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal()
		}
	}()
	scanner := bufio.NewScanner(file)
	scanner.Split(passportSplitFun)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
		passport := oneLine(scanner.Text())
		passports = append(passports, passport)
	}
	return passports
}

func isValid(field string, content string) bool {
	fmt.Println(field + " - " + content)
	switch field {
	case "byr":
		year, err := strconv.Atoi(content)
		if err == nil && year >= 1920 && year <= 2002 {
			return true
		} else {
			fmt.Println("false byr")
			return false
		}
	case "iyr":
		year, err := strconv.Atoi(content)
		if err == nil && year >= 2010 && year <= 2020 {
			return true
		} else {
			fmt.Println("false iyr")
			return false
		}
	case "eyr":
		year, err := strconv.Atoi(content)
		if err == nil && year >= 2020 && year <= 2030 {
			return true
		} else {
			fmt.Println("false eyr")
			return false
		}
	case "hgt":
		re := regexp.MustCompile("([0-9]+)((cm)|(in))")
		matches := re.FindStringSubmatch(content)
		if matches != nil {
			fmt.Println(matches)
			height, err := strconv.Atoi(matches[1])
			if err == nil {
				if (matches[2] == "cm" && height >= 150 && height <= 193) ||
					(matches[2] == "in" && height >= 59 && height <= 76) {
					return true
				}
			}
		}
		return false
	case "hcl":
		re := regexp.MustCompile("^#[0-9a-f]{6}$")
		if re.Match([]byte(content)) {
			return true
		}
		fmt.Println("false hcl")
		return false
	case "ecl":
		colors := []string{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
		for _, c := range colors {
			if content == c {
				return true
			}
		}
		fmt.Println("false ecl")
		return false
	case "pid":
		re := regexp.MustCompile("^[0-9]{9}$")
		if re.Match([]byte(content)) {
			return true
		}
		fmt.Println("false pid")
		return false
	case "cid":
		return true
	}
	fmt.Println("false default")
	return false
}

func goodPassports(passports []string) int {
	valids := 0
	req := []string{"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
PassportLoop:
	for _, pass := range passports {
		for _, rq := range req {
			re := regexp.MustCompile(rq + ":([a-z0-9#]+)")
			requiredMatch := re.FindStringSubmatch(pass)
			if requiredMatch == nil {
				continue PassportLoop
			} else {
				if !isValid(rq, requiredMatch[1]) {
					continue PassportLoop
				}
			}
		}
		valids++
	}
	return valids
}

func main() {
	passes := readPassports("input")
	log.Print("***** Answer *****")
	for _, p := range passes {
		fmt.Printf("Pass: %q\n", p)
	}
	fmt.Printf("%d\n", goodPassports(passes))
	return
}
