package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()
	scanner := bufio.NewScanner(file)
	var highest int64 = 0
	for scanner.Scan() {
		pass := scanner.Text()
		row := pass[:7]
		col := pass[7:]
		row = strings.ReplaceAll(row, "F", "0")
		row = strings.ReplaceAll(row, "B", "1")
		col = strings.ReplaceAll(col, "L", "0")
		col = strings.ReplaceAll(col, "R", "1")
		rowInt, err := strconv.ParseInt(row, 2, 8)
		if err != nil {
			log.Fatal(err)
		}
		colInt, err := strconv.ParseInt(col, 2, 4)
		if err != nil {
			log.Fatal(err)
		}
		passID := rowInt*8 + colInt
		if passID > highest {
			highest = passID
		}
	}
	fmt.Println(highest)
}
