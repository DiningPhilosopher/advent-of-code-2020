package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()
	scanner := bufio.NewScanner(file)
	var passes []int64
	for scanner.Scan() {
		pass := scanner.Text()
		row := pass[:7]
		col := pass[7:]
		row = strings.ReplaceAll(row, "F", "0")
		row = strings.ReplaceAll(row, "B", "1")
		col = strings.ReplaceAll(col, "L", "0")
		col = strings.ReplaceAll(col, "R", "1")
		rowInt, err := strconv.ParseInt(row, 2, 8)
		if err != nil {
			log.Fatal(err)
		}
		colInt, err := strconv.ParseInt(col, 2, 4)
		if err != nil {
			log.Fatal(err)
		}
		passID := rowInt*8 + colInt
		passes = append(passes, passID)
	}
	sort.Slice(passes, func(i, j int) bool { return passes[i] < passes[j] })
	var prev int64 = -2
	for _, checker := range passes {
		if prev == -2 {
			prev = checker
			continue
		} else if prev != checker-1 {
			fmt.Println(checker - 1)
		}
		prev = checker
	}
}
