#!/bin/bash
if [[ $# -ne 1 && $# -ne 2 ]]; then
  echo "This script runs a go-file when a certain file is written to. The first argument is the go-file to run. If you provide a second argument, this is the file to watch for changes. If no second argument is given, the go-file from the first argument will be watched." 
  exit 1
fi

while true
do
  if [[ $# -eq 1 ]]; then
    go run $1  
    inotifywait -e close_write $1
  elif [[ $# -eq 2 ]]; then 
    go run $1  
    inotifywait -e close_write $2
  fi
done
