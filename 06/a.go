package main

import (
	"fmt"
	fileutil "gitlab.com/DiningPhilosopher/go/fileutil"
)

func removeDuplicateChars(in string) string {
	keys := make(map[rune]bool)
	uniques := []rune{}
	for _, c := range in {
		if _, alreadyAdded := keys[c]; !alreadyAdded {
			uniques = append(uniques, c)
			keys[c] = true
		}
	}
	return string(uniques)
}

func main() {
	lines := fileutil.ReadLines("input")
	singleLines := make([]string, 1)
	current := 0
	for _, s := range lines {
		if len(s) == 0 {
			singleLines = append(singleLines, "")
			current++
		} else {
			singleLines[current] = singleLines[current] + s
		}
	}
	uniqueLines := []string{}
	for _, s := range singleLines {
		uniqueLines = append(uniqueLines, removeDuplicateChars(s))
	}
	total := 0
	for _, u := range uniqueLines {
		total += len(u)
	}
	fmt.Println(total)
}
