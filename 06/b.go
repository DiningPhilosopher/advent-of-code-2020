package main

import (
	"fmt"
	fileutil "gitlab.com/DiningPhilosopher/go/fileutil"
	"sort"
)

func sortRunes(runeSlice []rune) {
	sort.Slice(runeSlice, func(i, j int) bool {
		return runeSlice[i] < runeSlice[j]
	})
}

func intersection(set1 []rune, set2 []rune) []rune {
	sortRunes(set1)
	sortRunes(set2)
	idx1, idx2 := 0, 0
	result := []rune{}
	for idx1 < len(set1) && idx2 < len(set2) {
		r1 := set1[idx1]
		r2 := set2[idx2]
		if r1 == r2 {
			result = append(result, r1)
			idx1++
			idx2++
		} else if r1 < r2 {
			idx1++
		} else {
			idx2++
		}
	}
	return result
}

func main() {
	lines := fileutil.ReadLines("input")
	common := []rune{}
	newGroup := true
	total := 0
	for _, s := range lines {
		if len(s) == 0 {
			total += len(common)
			common = []rune{}
			newGroup = true
		} else {
			if newGroup {
				newGroup = false
				common = []rune(s)
			} else {
				common = intersection(common, []rune(s))
			}
		}
	}
	fmt.Println(total)
}
