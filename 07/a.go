package main

import (
	"fmt"
	slice "github.com/stretchr/stew/slice"
	fileutil "gitlab.com/DiningPhilosopher/go/fileutil"
	"log"
	"regexp"
	"strconv"
)

func getContained(line string) (string, map[string]int64) {
	re := regexp.MustCompile("([a-z ]+) bags contain(.*)")
	matches := re.FindStringSubmatch(line)
	container := matches[1]
	containedSubstr := matches[2]
	reContained := regexp.MustCompile("(?:([0-9]) )([a-z ]+) bags?[,|\\.]")
	containedMatches := reContained.FindAllStringSubmatch(containedSubstr, -1)
	containedMap := map[string]int64{}
	for _, c := range containedMatches {
		var number int64 = 0
		var err error
		if len(c[1]) > 0 {
			number, err = strconv.ParseInt(c[1], 10, 32)
			if err != nil {
				log.Fatal(err)
			}
		}
		containedMap[c[2]] = number
	}
	return container, containedMap
}

func oneTransitiveContains(container string, direct map[string][]string, start bool, visited []string) []string {
	result := []string{}
	visited = append(visited, container)
	if !start {
		result = append(result, container)
	}
	for _, contained := range direct[container] {
		if !slice.Contains(visited, contained) {
			transitive := oneTransitiveContains(contained, direct, false, visited)
			for _, transi := range transitive {
				result = append(result, transi)
			}
		}
	}
	return result
}

func transitiveContains(direct map[string][]string) map[string][]string {
	result := map[string][]string{}
	for container, _ := range direct {
		thisOneEventually := oneTransitiveContains(container, direct, true, make([]string, 0))
		result[container] = thisOneEventually
	}
	return result
}

func main() {
	lines := fileutil.ReadLines("input")
	contains := map[string][]string{}
	for _, l := range lines {
		container, new_contained := getContained(l)
		for color, _ := range new_contained {
			if !slice.Contains(contains[container], color) {
				contains[container] = append(contains[container], color)
			}
		}
	}
	eventuallyContains := transitiveContains(contains)
	result := []string{}
	for container, contained := range eventuallyContains {
		if slice.Contains(contained, "shiny gold") {
			result = append(result, container)
		}
	}
	fmt.Println(result)
	fmt.Println(len(result))
}
