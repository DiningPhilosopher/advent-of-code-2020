package main

import (
	"fmt"
	slice "github.com/stretchr/stew/slice"
	fileutil "gitlab.com/DiningPhilosopher/go/fileutil"
	"log"
	"regexp"
	"strconv"
)

type colorAndNumber struct {
	color  string
	number int64
}

func getContained(line string) (string, map[string]int64) {
	re := regexp.MustCompile("([a-z ]+) bags contain(.*)")
	matches := re.FindStringSubmatch(line)
	container := matches[1]
	containedSubstr := matches[2]
	reContained := regexp.MustCompile("(?:([0-9]) )([a-z ]+) bags?[,|\\.]")
	containedMatches := reContained.FindAllStringSubmatch(containedSubstr, -1)
	containedMap := map[string]int64{}
	for _, c := range containedMatches {
		var number int64 = 0
		var err error
		if len(c[1]) > 0 {
			number, err = strconv.ParseInt(c[1], 10, 32)
			if err != nil {
				log.Fatal(err)
			}
		}
		containedMap[c[2]] = number
	}
	return container, containedMap
}

func numberContained(container string, direct map[string][]colorAndNumber, totalContained map[string]int64) int64 {
	if val, alreadyCalculated := totalContained[container]; alreadyCalculated {
		return val
	}
	var result int64 = 0
	if _, colorExists := direct[container]; !colorExists {
		fmt.Println("Color does not exist!")
		return 0
	}
	if _, colorExists := direct[container]; colorExists && len(direct[container]) == 0 {
		return 0
	}
	for _, cAndN := range direct[container] {
		inContainedBag := numberContained(cAndN.color, direct, totalContained)
		// 		fmt.Println(container, cAndN, inContainedBag)
		result += cAndN.number * (1 + inContainedBag)
	}
	totalContained[container] = result
	return result
}

func main() {
	lines := fileutil.ReadLines("input")
	contains := map[string][]colorAndNumber{}
	for _, l := range lines {
		container, new_contained := getContained(l)
		if len(new_contained) == 0 {
			contains[container] = make([]colorAndNumber, 0)
		}
		for color, num := range new_contained {
			if !slice.Contains(contains[container], color) {
				contains[container] = append(contains[container], colorAndNumber{color: color, number: num})
			}
		}
	}
	fmt.Println("shiny gold contains", numberContained("shiny gold", contains, map[string]int64{}))
}
