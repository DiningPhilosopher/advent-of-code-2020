package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

func doInstr(instr []string, pc *int64, acc *int64) {
	switch instr[0] {
	case "jmp":
		add, _ := strconv.ParseInt(instr[1], 10, 64)
		*pc = *pc + add
	case "acc":
		add, _ := strconv.ParseInt(instr[1], 10, 64)
		*acc += add
		*pc += 1
	case "nop":
		*pc += 1
	}
}

func main() {
	lines := fileutil.ReadLines("input")
	instrs := [][]string{}
	for _, l := range lines {
		opAndArg := strings.Split(l, " ")
		instrs = append(instrs, opAndArg)
	}
	var acc int64 = 0
	var pc int64 = 0
	visited := map[int64]bool{}
	for {
		if _, looped := visited[pc]; !looped {
			visited[pc] = true
			doInstr(instrs[pc], &pc, &acc)
			fmt.Println(pc)
		} else {
			break
		}
	}
	fmt.Println(acc)
}
