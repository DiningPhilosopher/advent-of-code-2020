package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

func doInstr(instr []string, pc *int64, acc *int64) {
	switch instr[0] {
	case "jmp":
		add, _ := strconv.ParseInt(instr[1], 10, 64)
		*pc = *pc + add
	case "acc":
		add, _ := strconv.ParseInt(instr[1], 10, 64)
		*acc += add
		*pc += 1
	case "nop":
		*pc += 1
	}
}

func correctedCopy(instrs [][]string, idx int64) [][]string {
	instrsCopy := make([][]string, len(instrs))
	for jdx := 0; jdx < len(instrs); jdx++ {
		instrsCopy[jdx] = []string{instrs[jdx][0], instrs[jdx][1]}
	}
	if instrsCopy[idx][0] == "nop" {
		instrsCopy[idx][0] = "jmp"
	} else if instrsCopy[idx][0] == "jmp" {
		instrsCopy[idx][0] = "nop"
	} else {
		return nil
	}
	return instrsCopy
}

func main() {
	lines := fileutil.ReadLines("input")
	instrs := [][]string{}
	for _, l := range lines {
		opAndArg := strings.Split(l, " ")
		instrs = append(instrs, opAndArg)
	}
	var corrIdx int64
	for corrIdx = 0; corrIdx < int64(len(instrs)); corrIdx++ {
		corrInstrs := correctedCopy(instrs, corrIdx)
		if corrInstrs != nil {
			var acc int64 = 0
			var pc int64 = 0
			visited := map[int64]bool{}
			for {
				if pc == int64(len(corrInstrs)) {
					fmt.Println("Result after correction: ", acc)
					return
				} else if _, looped := visited[pc]; !looped {
					visited[pc] = true
					doInstr(corrInstrs[pc], &pc, &acc)
				} else {
					break
				}
			}
		}
	}
}
