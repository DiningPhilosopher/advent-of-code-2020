package main

import (
	"fmt"
	"github.com/stretchr/stew/slice"
	fileutil "gitlab.com/DiningPhilosopher/go/fileutil"
	"strconv"
)

func validateNum(nums []int, idx *int, summerMap map[int][]int) bool {
	numToValidate := nums[*idx]
	for _, sums := range summerMap {
		if slice.Contains(sums, numToValidate) {
			// Valid! Update variables for next call to validateNum
			delete(summerMap, *idx-25)
			for jdx := *idx - 24; jdx < *idx; jdx++ {
				for kdx := jdx + 1; kdx <= *idx; kdx++ {
					summerMap[jdx] = append(summerMap[jdx], nums[jdx]+nums[kdx])
				}
			}
			*idx++
			return true
		}
	}
	return false
}

func min(values []int) int {
	min := values[0]
	for _, v := range values {
		if v < min {
			min = v
		}
	}
	return min
}

func max(values []int) int {
	max := values[0]
	for _, v := range values {
		if v > max {
			max = v
		}
	}
	return max
}

// Assumes a solution exists, no bounds checking at the end
func part2(nums []int, target int) int {
	first, last := 0, 1
	window := []int{nums[0], nums[1]}
	currentSum := nums[0] + nums[1]
	for last < len(nums) {
		if target == currentSum {
			return min(window) + max(window)
		} else if target > currentSum {
			last++
			window = append(window, nums[last])
			currentSum += nums[last]
		} else {
			currentSum -= nums[first]
			_, window = window[0], window[1:]
			first++
			if last == first {
				last++
				window = append(window, nums[last])
				currentSum += nums[last]
			}
		}
	}
	return -1
}

func main() {
	lines := fileutil.ReadLines("input")
	nums := make([]int, len(lines))
	for idx := range lines {
		newInt, _ := strconv.Atoi(lines[idx])
		nums[idx] = newInt
	}
	// Maps the first number contributing to a valid sum to a slice of the valid
	// sums that it contributes to.
	// As we advance through the numbers, we drop the old numbers and their sums,
	// and add the sum of the new number and old numbers to each of the
	// corresponding old number entries in the map.
	firstSummer := make(map[int][]int, 25)
	var idx int
	for idx := 0; idx < 25; idx++ {
		for jdx := idx + 1; jdx < 25; jdx++ {
			firstSummer[idx] = append(firstSummer[idx], nums[idx]+nums[jdx])
		}
	}
	idx = 25
	for idx < len(nums) {
		if !validateNum(nums, &idx, firstSummer) {
			invalid := nums[idx]
			fmt.Println("First invalid number: ", invalid)
			result2 := part2(nums, invalid)
			fmt.Println("Sum of first and last contiguous numbers:", result2)
			return
		}
	}
}
