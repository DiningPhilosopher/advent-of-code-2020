package main

import (
	"fmt"
	"gitlab.com/DiningPhilosopher/go/fileutil"
	"sort"
	"strconv"
)

func waysReachable(idx int, nums []int, cache map[int]int) int {
	if idx == 0 {
		return 1
	} else {
		cached, present := cache[idx]
		if present {
			return cached
		}
		result := 0
		num := nums[idx]
		for jdx := idx - 1; jdx >= 0; jdx-- {
			prevNum := nums[jdx]
			if num-prevNum <= 3 {
				result += waysReachable(jdx, nums, cache)
			} else {
				break
			}
		}
		cache[idx] = result
		return result
	}
	return 0
}

func main() {
	lines := fileutil.ReadLines("input")
	// Start with one zero for socket jolt rating
	nums := make([]int, 1, len(lines))
	for _, l := range lines {
		converted, _ := strconv.Atoi(l)
		nums = append(nums, converted)
	}
	sort.Ints(nums)
	// Add for the built-in adapter
	nums = append(nums, nums[len(nums)-1]+3)
	oneDiffs, threeDiffs := 0, 0
	for idx := 0; idx < len(nums)-1; idx++ {
		diff := nums[idx+1] - nums[idx]
		if diff == 1 {
			oneDiffs++
		} else if diff == 3 {
			threeDiffs++
		}
	}
	fmt.Println("Onediffs ", oneDiffs)
	fmt.Println("Threediffs", threeDiffs)
	fmt.Println("Product", oneDiffs*threeDiffs)
	// Part 2
	// Count how many ways of going to the next adapter each adapter has
	cache := map[int]int{}
	reachableWays := waysReachable(len(nums)-1, nums, cache)
	fmt.Println("Adapter combinations possible: ", reachableWays)
}
