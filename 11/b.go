package main

import (
	"fmt"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
	"gitlab.com/DiningPhilosopher/go/sliceutil"
)

type neighborCounts struct {
	occupied int
	empty    int
	floor    int
}

func firstSeen(lines []string, row, col, dr, dc int) byte {
	r := row + dr
	c := col + dc
	for r >= 0 && r < len(lines) && c >= 0 && c < len(lines[0]) {
		if lines[r][c] != byte('.') {
			return lines[r][c]
		}
		r += dr
		c += dc
	}
	return byte('.')
}

func getVisibleNeighbors(lines []string, row, col int) neighborCounts {
	neighs := neighborCounts{}
	for r := -1; r <= 1; r++ {
		for c := -1; c <= 1; c++ {
			if !(r == 0 && c == 0) {
				seenInDir := firstSeen(lines, row, col, r, c)
				switch seenInDir {
				case byte('L'):
					neighs.empty++
				case byte('#'):
					neighs.occupied++
				case byte('.'):
					neighs.empty++
				}
			}
		}
	}
	return neighs
}

func replaceAtIdx(original string, idx int, replacement rune) string {
	result := []rune(original)
	result[idx] = replacement
	return string(result)
}

func nextRound(lines []string) ([]string, bool) {
	changed := false
	nextLines := sliceutil.CopyStringSlice(lines)
	for row, l := range lines {
		for col := range l {
			neighCounts := getVisibleNeighbors(lines, row, col)
			if lines[row][col] == byte('#') {
				if neighCounts.occupied >= 5 {
					nextLines[row] = replaceAtIdx(nextLines[row], col, rune('L'))
					changed = true
				}
			} else if lines[row][col] == byte('L') {
				if neighCounts.occupied == 0 {
					nextLines[row] = replaceAtIdx(nextLines[row], col, rune('#'))
					changed = true
				}
			}
		}
	}
	return nextLines, changed
}

func countOccupied(lines []string) int {
	result := 0
	for _, l := range lines {
		result += strings.Count(l, "#")
	}
	return result
}

func main() {
	lines := fileutil.ReadLines("input")
	changed := true
	testcounter := 0
	for {
		lines, changed = nextRound(lines)
		if !changed {
			break
		}
		testcounter++
		if testcounter == 100 {
			break
		}
	}
	fmt.Println(countOccupied(lines))
}
