package main

import (
	"fmt"
	"math"
	"strconv"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

type vector2 struct {
	x float64
	y float64
}

func (v vector2) Length() float64 {
	return math.Sqrt(v.x*v.x + v.y*v.y)
}

func (v vector2) Angle() float64 {
	return math.Atan2(v.y, v.x)
}

func (v vector2) Add(v2 vector2) vector2 {
	return vector2{
		x: v.x + v2.x,
		y: v.y + v2.y,
	}
}
func (v vector2) Times(factor float64) vector2 {
	return vector2{
		x: factor * v.x,
		y: factor * v.y,
	}
}

func (v vector2) Rotate(radians float64) vector2 {
	r := v.Length()
	oldAngle := v.Angle()
	newAngle := oldAngle + radians
	return vector2{
		x: r * math.Cos(newAngle),
		y: r * math.Sin(newAngle),
	}
}

func main() {
	lines := fileutil.ReadLines("input")
	dir := vector2{x: 1, y: 0}
	pos := vector2{}
	for _, l := range lines {
		letter := l[0]
		number, _ := strconv.Atoi(string(l[1:]))
		switch letter {
		case byte('L'):
			dir = dir.Rotate(float64(number) * math.Pi / 180.0)
		case byte('R'):
			dir = dir.Rotate(-1 * float64(number) * math.Pi / 180.0)
		case byte('F'):
			pos = pos.Add(dir.Times(float64(number)))
		case byte('N'):
			pos = pos.Add(vector2{y: 1}.Times(float64(number)))
		case byte('S'):
			pos = pos.Add(vector2{y: -1}.Times(float64(number)))
		case byte('E'):
			pos = pos.Add(vector2{x: 1}.Times(float64(number)))
		case byte('W'):
			pos = pos.Add(vector2{x: -1}.Times(float64(number)))
		}
	}
	fmt.Println(math.Abs(pos.x) + math.Abs(pos.y))
}
