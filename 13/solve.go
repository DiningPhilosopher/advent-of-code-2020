package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

// From https://play.golang.org/p/SmzvkDjYlb
// greatest common divisor (GCD) via Euclidean algorithm
func GCD(a, b int64) int64 {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

// find Least Common Multiple (LCM) via GCD
func LCM(a, b int64, integers ...int64) int64 {
	result := a * b / GCD(a, b)
	for i := 0; i < len(integers); i++ {
		result = LCM(result, integers[i])
	}
	return result
}

func main() {
	lines := fileutil.ReadLines("input")
	t0, _ := strconv.ParseInt(lines[0], 10, 64)
	factors := strings.Split(lines[1], ",")
	var bestNextTime int64 = math.MaxInt64
	var bestBus int64
	timeTable := make(map[int]int64)
	var periods []int64
	for idx, factorStr := range factors {
		factor, err := strconv.ParseInt(factorStr, 10, 64)
		if err == nil {
			timeTable[idx] = factor
			periods = append(periods, factor)
			nextTime := (factor - t0%factor) % factor
			if nextTime < bestNextTime {
				bestBus = factor
				bestNextTime = nextTime
			}
		}
	}
	fmt.Println("Part A: ", bestNextTime*bestBus)
	var periodAll int64 = 1
	var earliest int64 = 0
	for offset, period := range timeTable {
		// Find new earliest such that (earliest + offset) % period == 0
		// For this, advance earliest in steps of periodAll to satisfy earlier constraints.
		for (earliest+int64(offset))%period != 0 {
			earliest += periodAll
		}
		periodAll = LCM(period, periodAll)
	}
	fmt.Println("Part B: ", earliest)
}
