package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

func main() {
	lines := fileutil.ReadLines("input")
	var andMask, orMask, rvalue int64
	var addr int
	memory := map[int]int64{}
	for _, l := range lines {
		tokens := strings.Split(l, " ")
		if tokens[0] == "mask" {
			// Mask with ones for Xes, sets masking zeroes
			andMask, _ = strconv.ParseInt(strings.ReplaceAll(tokens[2], "X", "1"), 2, 64)
			// Mask with zeroes for Xes, sets masking ones
			orMask, _ = strconv.ParseInt(strings.ReplaceAll(tokens[2], "X", "0"), 2, 64)
		} else {
			addr, _ = strconv.Atoi(tokens[0][4 : len(tokens[0])-1])
			rvalue, _ = strconv.ParseInt(tokens[2], 10, 64)
			memory[addr] = (rvalue & andMask) | orMask
		}
	}
	var total int64 = 0
	for _, v := range memory {
		total += v
	}
	fmt.Println(total)
}
