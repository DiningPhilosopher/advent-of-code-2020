package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

func main() {
	lines := fileutil.ReadLines("input")
	var onesMask, floatMask, rvalue int64
	var addr int64
	memory := map[int64]int64{}
	for _, l := range lines {
		tokens := strings.Split(l, " ")
		if tokens[0] == "mask" {
			// Mask with only the ones
			onesMask, _ = strconv.ParseInt(strings.ReplaceAll(tokens[2], "X", "0"), 2, 64)
			// Mask with ones for all the floating bits
			floatMaskStr := strings.ReplaceAll(tokens[2], "1", "0")
			floatMaskStr = strings.ReplaceAll(floatMaskStr, "X", "1")
			floatMask, _ = strconv.ParseInt(floatMaskStr, 2, 64)
		} else {
			addr, _ = strconv.ParseInt(tokens[0][4:len(tokens[0])-1], 10, 64)
			addrWithoutMask := (addr | onesMask) & ^floatMask // clear mask bits
			rvalue, _ = strconv.ParseInt(tokens[2], 10, 64)
			allAddresses := []int64{addrWithoutMask}
			for bitIdx := 0; bitIdx <= 35; bitIdx++ {
				if floatMask&(1<<int64(bitIdx)) != 0 {
					// the bit at bitIdx is floating, add copies of previous addresses with one set
					newAddresses := []int64{}
					for _, a := range allAddresses {
						newAddresses = append(newAddresses, a|1<<int64(bitIdx))
					}
					allAddresses = append(allAddresses, newAddresses...)
				}
			}
			for _, a := range allAddresses {
				memory[a] = rvalue
			}
		}
	}
	var total int64 = 0
	for _, v := range memory {
		total += v
	}
	fmt.Println(total)
}
