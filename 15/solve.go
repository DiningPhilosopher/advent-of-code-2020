package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

func main() {
	lines := fileutil.ReadLines("input")
	numStrs := strings.Split(lines[0], ",")
	numToLastIdx := map[int]int{}
	numToBeforeIdx := map[int]int{}
	idx, latestNum := 0, 0
	for _, numStr := range numStrs {
		num, _ := strconv.Atoi(numStr)
		if _, lastPresent := numToLastIdx[num]; lastPresent {
			numToBeforeIdx[num] = numToLastIdx[num]
		}
		numToLastIdx[num] = idx
		latestNum = num
		idx++
	}
	// idx = 2020 for part A
	for idx < 30000000 {
		if beforeIdx, present := numToBeforeIdx[latestNum]; present {
			latestNum = numToLastIdx[latestNum] - beforeIdx
		} else {
			latestNum = 0
		}
		if _, lastPresent := numToLastIdx[latestNum]; lastPresent {
			numToBeforeIdx[latestNum] = numToLastIdx[latestNum]
		}
		numToLastIdx[latestNum] = idx
		idx++
	}
	fmt.Println(latestNum)
}
