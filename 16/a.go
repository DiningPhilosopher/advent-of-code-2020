package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

type interval struct {
	min int
	max int
}

func main() {
	lines := fileutil.ReadLines("input")
	oneLine := strings.Join(lines[:], "\n")
	sections := strings.Split(oneLine, "\n\n")
	fieldsHeader := strings.Split(sections[0], "\n")
	validRanges := []interval{}
	for _, fieldLine := range fieldsHeader {
		twoNumbersRegex := regexp.MustCompile("([0-9]+)-([0-9]+)")
		match := twoNumbersRegex.FindAllStringSubmatch(fieldLine, -1)
		for _, subMatch := range match {
			minVal, _ := strconv.Atoi(subMatch[1])
			maxVal, _ := strconv.Atoi(subMatch[2])
			validRanges = append(validRanges, interval{min: minVal, max: maxVal})
		}
	}
	allTicketNumStrs := strings.Split(sections[2], "\n")[1:]
	allTicketNums := strings.Split(strings.Join(allTicketNumStrs[:], ","), ",")
	sum := 0
	for _, ticketStr := range allTicketNums {
		ticketNum, _ := strconv.Atoi(ticketStr)
		valid := false
		for _, vr := range validRanges {
			if ticketNum >= vr.min && ticketNum <= vr.max {
				valid = true
				break
			}
		}
		if !valid {
			sum += ticketNum
		}
	}
	fmt.Println(sum)
}
