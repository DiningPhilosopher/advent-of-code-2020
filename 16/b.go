package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

type interval struct {
	min int
	max int
}

func pickField(idx int, field string, idxToFields map[int][]string) map[int][]string {
	newIdxToFields := map[int][]string{}
	for jdx, fields := range idxToFields {
		if jdx != idx {
			newFields := []string{}
			for _, f := range fields {
				if f != field {
					newFields = append(newFields, f)
				}
			}
			newIdxToFields[jdx] = newFields
		}
	}
	return newIdxToFields
}

func getValidAllocation(idx int, idxToValidFields map[int][]string) []string {
	if len(idxToValidFields) == 0 {
		return make([]string, 0)
	}
	validFields, present := idxToValidFields[idx]
	if !present {
		fmt.Println("Error: idx should be in map.")
	}
	if len(validFields) == 0 {
		return nil
	}
	var recursionResult []string
	var result []string
	for _, field := range validFields {
		newIdxToFields := pickField(idx, field, idxToValidFields)
		recursionResult = getValidAllocation(idx+1, newIdxToFields)
		if recursionResult != nil {
			result = []string{field}
			result = append(result, recursionResult...)
			break
		}
	}
	return result
}

func inIntervalSet(num int, ivalSet []interval) bool {
	for _, ival := range ivalSet {
		if num >= ival.min && num <= ival.max {
			return true
		}
	}
	return false
}
func main() {
	lines := fileutil.ReadLines("input")
	oneLine := strings.Join(lines[:], "\n")
	sections := strings.Split(oneLine, "\n\n")
	fieldsHeader := strings.Split(sections[0], "\n")
	validRanges := map[string][]interval{}
	for _, fieldLine := range fieldsHeader {
		fieldName := strings.Split(fieldLine, ":")[0]
		twoNumbersRegex := regexp.MustCompile("([0-9]+)-([0-9]+)")
		match := twoNumbersRegex.FindAllStringSubmatch(fieldLine, -1)
		fieldRanges := []interval{}
		for _, subMatch := range match {
			minVal, _ := strconv.Atoi(subMatch[1])
			maxVal, _ := strconv.Atoi(subMatch[2])
			fieldRanges = append(fieldRanges, interval{min: minVal, max: maxVal})
		}
		validRanges[fieldName] = fieldRanges
	}
	allTicketStrings := strings.Split(sections[2], "\n")[1:]
	validTickets := [][]int{}
	for _, ticketStr := range allTicketStrings {
		ticketNumStr := strings.Split(ticketStr, ",")
		ticketNumSlice := []int{}
		allValid := true
		for _, tns := range ticketNumStr {
			ticketNum, _ := strconv.Atoi(tns)
			ticketNumSlice = append(ticketNumSlice, ticketNum)
			valid := false
			for _, fieldValidRanges := range validRanges {
				if inIntervalSet(ticketNum, fieldValidRanges) {
					valid = true
					goto ValidityKnown
				}
			}
		ValidityKnown:
			if !valid {
				allValid = false
				break
			}
		}
		if allValid {
			validTickets = append(validTickets, ticketNumSlice)
		}
	}
	idxToValidFields := map[int][]string{}
	// Create list of field names that are valid for each column field idxs.
	for idx := range validTickets[0] {
		validFields := []string{}
		for fieldName, fieldRanges := range validRanges {
			fieldIsValid := true
			for _, ticket := range validTickets {
				numToCheck := ticket[idx]
				if !inIntervalSet(numToCheck, fieldRanges) {
					fieldIsValid = false
				}
			}
			if fieldIsValid {
				validFields = append(validFields, fieldName)
			}
		}
		idxToValidFields[idx] = validFields
	}
	resultIdxToValidFields := getValidAllocation(0, idxToValidFields)
	fmt.Println("valid allocation:", resultIdxToValidFields)
	myTicketStr := strings.Split(sections[1], "\n")[1]
	myTicketNumStrings := strings.Split(myTicketStr, ",")
	product := 1
	numberDeparts := 0
	for idx, field := range resultIdxToValidFields {
		if strings.HasPrefix(field, "departure") {
			numberDeparts++
			departureNum, _ := strconv.Atoi(myTicketNumStrings[idx])
			product *= departureNum
		}
	}
	fmt.Println(numberDeparts, product)
}
