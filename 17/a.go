package main

import (
	"fmt"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

type coordinate struct {
	x int
	y int
	z int
}

func nextCell(space map[coordinate]bool, coord coordinate) bool {
	neighsOn := 0
	for x := coord.x - 1; x <= coord.x+1; x++ {
		for y := coord.y - 1; y <= coord.y+1; y++ {
			for z := coord.z - 1; z <= coord.z+1; z++ {
				if !(x == coord.x && y == coord.y && z == coord.z) {
					if space[coordinate{x: x, y: y, z: z}] {
						neighsOn++
					}
				}
			}
		}
	}
	if space[coord] {
		if neighsOn == 2 || neighsOn == 3 {
			return true
		}
	} else {
		if neighsOn == 3 {
			return true
		}
	}
	return false
}

func nextSpace(space map[coordinate]bool, dimensionMax int) map[coordinate]bool {
	space2 := map[coordinate]bool{}
	for x := -6; x < dimensionMax; x++ {
		for y := -6; y < dimensionMax; y++ {
			for z := -6; z < dimensionMax; z++ {
				coord := coordinate{x: x, y: y, z: z}
				space2[coord] = nextCell(space, coord)
			}
		}
	}
	return space2
}

func main() {
	lines := fileutil.ReadLines("input")
	// Create the initial, empty pocket dimension.
	space := map[coordinate]bool{}
	for y, l := range lines {
		for x, char := range l {
			space[coordinate{x: x, y: y, z: 0}] = char == rune('#')
		}
	}
	dimensionMax := 20
	for iter := 0; iter < 6; iter++ {
		space = nextSpace(space, dimensionMax)
	}
	totalOn := 0
	for x := -6; x < dimensionMax; x++ {
		for y := -6; y < dimensionMax; y++ {
			for z := -6; z < dimensionMax; z++ {
				coord := coordinate{x: x, y: y, z: z}
				if space[coord] {
					totalOn++
				}
			}
		}
	}
	fmt.Println(totalOn)
}
