package main

import (
	"fmt"
	"log"
	"strconv"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

const (
	_ = iota
	OP
	NUM
	BO
	BC
)

type Token struct {
	tt     int
	numVal int
	opVal  rune
}

type Tree struct {
	left  *Tree
	right *Tree
	token Token
}

func tokenize(expr string) []Token {
	tokenList := []Token{}
	for _, ch := range expr {
		if ch == rune(' ') {
			continue
		} else {
			var newToken Token
			if ch == rune('(') {
				newToken = Token{tt: BO}
			} else if ch == rune(')') {
				newToken = Token{tt: BC}
			} else if digit, err := strconv.Atoi(string(ch)); err == nil {
				newToken = Token{tt: NUM, numVal: digit}
			} else {
				newToken = Token{tt: OP, opVal: ch}
			}
			tokenList = append(tokenList, newToken)
		}
	}
	return tokenList
}

func popFactor(tokenList []Token) (*Tree, []Token) {
	if len(tokenList) == 0 {
		return &Tree{}, []Token{}
	}
	idx := 0
	toMatch := 0
	for idx < len(tokenList) && (tokenList[idx].opVal != rune('*') || toMatch > 0) {
		switch tokenList[idx].tt {
		case BO:
			toMatch++
		case BC:
			toMatch--
		}
		idx++
	}
	tokensRemaining := []Token{}
	if idx < len(tokenList) {
		tokensRemaining = tokenList[idx:]
	}
	return createAST(tokenList[0:idx], nil), tokensRemaining
}

func popTerm(tokenList []Token) (*Tree, []Token) {
	tokensRemaining := []Token{}
	if len(tokenList) == 0 {
		return &Tree{}, []Token{}
	}
	if tokenList[0].tt == NUM {
		if len(tokenList) > 1 {
			tokensRemaining = tokenList[1:]
		}
		return &Tree{token: tokenList[0]}, tokensRemaining
	} else if tokenList[0].tt == BO {
		bracketed := []Token{}
		toMatch := 1
		idx := 0
		for toMatch > 0 {
			idx++
			nextToken := tokenList[idx]
			if nextToken.tt == BC {
				toMatch--
			} else if nextToken.tt == BO {
				toMatch++
			}
			if toMatch > 0 {
				bracketed = append(bracketed, nextToken)
			}

		}
		if len(tokenList) > idx+1 {
			tokensRemaining = tokenList[idx+1:]
		}
		return createAST(bracketed, &Tree{}), tokensRemaining
	} else {
		log.Fatal("Operator should be followed by number of opening bracket.")
		return &Tree{}, []Token{}
	}
}

func createAST(tokenList []Token, left *Tree) *Tree {
	if len(tokenList) == 0 {
		return left
	}
	token := tokenList[0]
	if token.tt == NUM {
		return createAST(tokenList[1:], &Tree{token: token})
	} else if token.tt == OP {
		var tokensRemaining []Token
		var rightSubTree *Tree
		if token.opVal == rune('+') {
			rightSubTree, tokensRemaining = popTerm(tokenList[1:])
		} else if token.opVal == rune('*') {
			rightSubTree, tokensRemaining = popFactor(tokenList[1:])
		} else {
			log.Fatal("Unknown operator: ", token.opVal)
		}
		return createAST(tokensRemaining, &Tree{token: token, left: left, right: rightSubTree})
	} else if token.tt == BO {
		leftSubTree, tokensLeft := popTerm(tokenList)
		return createAST(tokensLeft, leftSubTree)
	}
	log.Fatal("Unknown token: ", token.tt)
	return &Tree{}
}

func evalTree(tree *Tree) int {
	if tree.token.tt == NUM {
		return tree.token.numVal
	} else {
		switch tree.token.opVal {
		case rune('+'):
			return evalTree(tree.left) + evalTree(tree.right)
		case rune('*'):
			return evalTree(tree.left) * evalTree(tree.right)
		}
	}
	return 0
}

func main() {
	lines := fileutil.ReadLines("input")
	sum := 0
	for _, l := range lines {
		tokens := tokenize(l)
		tree := createAST(tokens, &Tree{})
		sum += evalTree(tree)
	}
	fmt.Println(sum)
}
