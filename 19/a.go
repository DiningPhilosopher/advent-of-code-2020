package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
	"gitlab.com/DiningPhilosopher/go/sliceutil"
)

func main() {
	lines := fileutil.ReadLines("input_a")
	linesJoined := strings.Join(lines, "\n")
	rulesAndMsgs := strings.Split(linesJoined, "\n\n")
	msgs := rulesAndMsgs[1]
	ruleStrings := strings.Split(rulesAndMsgs[0], "\n")
	// map of rule numbers to regex strings (e.g. aaaa|baba)
	regexStrMap := map[int]string{}
	ruleMap := map[int][][]int{}
	// create ruleMap, mapping rule numbers to zero, one or two slices of rule numbers:
	// Zero slices: base rule, having an explicit character to match
	// One slice: sequence of rules to match
	// Two slices: two sequences of rules, one of which is to match
	for _, r := range ruleStrings {
		ruleStrRegex := regexp.MustCompile("([0-9]+): (.*)")
		numAndRuleStr := ruleStrRegex.FindStringSubmatch(r)
		ruleNum, _ := strconv.Atoi(numAndRuleStr[1])
		ruleStr := numAndRuleStr[2]
		firstAndSecond := strings.Split(ruleStr, "|")
		intRegex := regexp.MustCompile("[0-9]+")
		for _, ruleSet := range firstAndSecond {
			if intRegex.MatchString(ruleSet) {
				intMatches := intRegex.FindAllString(ruleSet, -1)
				ruleNumSlice := []int{}
				for _, r2 := range intMatches {
					r2num, _ := strconv.Atoi(r2)
					ruleNumSlice = append(ruleNumSlice, r2num)
				}
				ruleMap[ruleNum] = append(ruleMap[ruleNum], ruleNumSlice)
			} else {
				regexStrMap[ruleNum] = string(ruleStr[1])
			}
		}
	}
	// map rule numbers to other rule numbers that use them.
	depMap := map[int][]int{}
	for ruleNum, depSets := range ruleMap {
		for _, ruleSetsUsed := range depSets {
			for _, ruleUsed := range ruleSetsUsed {
				if sliceutil.FindInt(depMap[ruleUsed], ruleNum) == -1 {
					depMap[ruleUsed] = append(depMap[ruleUsed], ruleNum)
				}
			}
		}
	}
	// create all regexes, iteratively resolving them
	allComplete := false
	for !allComplete {
		allComplete = true
		for maybeResolved, dependers := range depMap {
			if _, present := regexStrMap[maybeResolved]; present {
				for _, depender := range dependers {
					resolvedNew := tryResolve(regexStrMap, ruleMap, depender)
					if resolvedNew {
						allComplete = false
					}
				}
			}
		}
	}

	// keys := make([]int, 0, len(regexStrMap))
	// for k := range regexStrMap {
	// 	keys = append(keys, k)
	// }
	// sort.Ints(keys)
	// for _, k := range keys {
	// 	fmt.Println(k, regexStrMap[k])
	// }
	megaRegex := regexp.MustCompile("^" + regexStrMap[0] + "$")
	total := 0
	for msg_idx, msg := range strings.Split(msgs, "\n") {
		if megaRegex.MatchString(string(msg)) {
			total++
			msg_idx++
		}
	}
	fmt.Println(total)
}

func tryResolve(regexStrMap map[int]string, ruleMap map[int][][]int, depender int) bool {
	if _, present := regexStrMap[depender]; present {
		// already resolved
		return false
	}
	regexStr := ""
	// only resolve non-basic rules (i.e. rules composed of other rule(s))
	if ruleSets, present := ruleMap[depender]; present {
		// for each set of rules that the to-be-resolved rule depends on...
		// a "set of rules" is a sequence of rules that defines a rule, either before or after the OR (|)
		// in the input given, each rule thus has at most 2 rule sets, since there is at most one OR.
		for ruleSetIdx, ruleSet := range ruleSets {
			for _, ruleUsed := range ruleSet {
				if ruleStr, present := regexStrMap[ruleUsed]; present {
					regexStr = regexStr + "(" + ruleStr + ")"
				} else {
					return false
				}
			}
			if len(ruleSets) > 1 && ruleSetIdx == 0 {
				regexStr = regexStr + "|"
			}
		}
	}
	regexStrMap[depender] = regexStr
	return true
}
