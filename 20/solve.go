package main

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
	"gitlab.com/DiningPhilosopher/go/mathutil"
	"gitlab.com/DiningPhilosopher/go/stringutil"
)

var sideLen int
var blockIds []int
var blockMap map[int]*block
var sideToBlockIds map[int64][]int

type block struct {
	id int
	// binary representation of #=1 and .=0
	// order of the int64 always assumes
	// clockwise order of the bits, i.e.
	// top = left-right
	// right = top-bottom
	// bottom = right-left
	// left = down-bottom
	// This way the bit-order of a side stays the same while rotating.
	fullBlock *[]string
	sides     []int64
	// Current state is always as if first flipped left-to-right
	// and then rotated over rotation.
	rotation int
	flipped  bool
}

func (b *block) rotate(times int) {
	shortTimes := times % 4
	b.rotation = (b.rotation + shortTimes) % 4
	b.sides = append(b.sides[4-shortTimes:], b.sides[0:4-shortTimes]...)
}

func (b *block) flipLR() {
	b.sides[0] = sideReverseInt64(b.sides[0])
	b.sides[2] = sideReverseInt64(b.sides[2])
	b.sides[1] = sideReverseInt64(b.sides[3])
	b.sides[3] = sideReverseInt64(b.sides[1])
	if b.rotation == 1 {
		b.rotation = 3
	} else if b.rotation == 3 {
		b.rotation = 1
	}
	b.flipped = !b.flipped
}

func (b *block) flipUD() {
	b.sides[1] = sideReverseInt64(b.sides[1])
	b.sides[3] = sideReverseInt64(b.sides[3])
	b.sides[0] = sideReverseInt64(b.sides[2])
	b.sides[2] = sideReverseInt64(b.sides[0])
	if b.rotation == 2 {
		b.rotation = 0
	} else if b.rotation == 0 {
		b.rotation = 2
	}
	b.flipped = !b.flipped
}

func (b *block) fullBlockStrings() []string {
	flippedResult := []string{}
	for _, row := range *b.fullBlock {
		if b.flipped {
			flippedResult = append(flippedResult, stringutil.Reverse(row))
		} else {
			flippedResult = append(flippedResult, row)
		}
	}
	result := []string{}
	if b.rotation == 0 {
		return flippedResult
	} else if b.rotation == 2 {
		for rowIdx := len(flippedResult) - 1; rowIdx >= 0; rowIdx-- {
			result = append(result, stringutil.Reverse(flippedResult[rowIdx]))
		}
	} else {
		for idx := 0; idx < len(flippedResult); idx++ {
			result = append(result, "")
		}
		if b.rotation == 1 {
			for rowIdx := len(flippedResult) - 1; rowIdx >= 0; rowIdx-- {
				for newRowIdx := 0; newRowIdx < len(flippedResult[0]); newRowIdx++ {
					result[newRowIdx] += string(flippedResult[rowIdx][newRowIdx])
				}
			}
		} else if b.rotation == 3 {
			for rowIdx := 0; rowIdx < len(flippedResult); rowIdx++ {
				for newRowIdx := 0; newRowIdx < len(flippedResult[0]); newRowIdx++ {
					result[newRowIdx] += string(flippedResult[rowIdx][len(flippedResult[0])-1-newRowIdx])
				}
			}
		}
	}
	return result
}

func sideReverseInt64(num int64) int64 {
	return binStrToInt64(stringutil.Reverse(int64ToBinString(num)))
}

func int64ToBinString(num int64) string {
	out := ""
	for num > 0 {
		if num%2 == 1 {
			out = "#" + out
		} else {
			out = "." + out
		}
		num = num >> 1
	}
	for len(out) < sideLen {
		out = "." + out
	}
	return out
}

func binStrToInt64(binStr string) int64 {
	str2 := strings.ReplaceAll(binStr, "#", "1")
	str3 := strings.ReplaceAll(str2, ".", "0")
	intSide, _ := strconv.ParseInt(str3, 2, 64)
	return intSide
}

func canonicalSide(side int64) int64 {
	correctFlip := side
	sideFlip := binStrToInt64(stringutil.Reverse(int64ToBinString(side)))
	if side > sideFlip {
		correctFlip = sideFlip
	}
	return correctFlip
}

func createBlock(header int, fullBlock []string) *block {
	top := fullBlock[0]
	bottom := stringutil.Reverse(fullBlock[len(fullBlock)-1])
	left := ""
	right := ""
	for idx := 0; idx < len(fullBlock); idx++ {
		left = string(fullBlock[idx][0]) + left
		right += string(fullBlock[idx][len(fullBlock[idx])-1])
	}
	intSides := []int64{}
	for _, strSide := range []string{top, right, bottom, left} {
		// translate strings to binary numbers
		intSide := binStrToInt64(strSide)
		intSides = append(intSides, intSide)
	}
	newBlock := block{
		id:        header,
		fullBlock: &fullBlock,
		sides:     intSides,
		rotation:  0,
	}
	return &newBlock
}

func createSideToBlockMap(blockMap map[int]*block) map[int64][]int {
	sideToBlock := map[int64][]int{}
	for id, block := range blockMap {
		for _, side := range block.sides {
			// use flipped version of side that is the lowest
			correctFlip := canonicalSide(side)
			sideFlip := binStrToInt64(stringutil.Reverse(int64ToBinString(side)))
			if side > sideFlip {
				correctFlip = sideFlip
			}
			sideToBlock[correctFlip] = append(sideToBlock[correctFlip], id)
		}
	}
	return sideToBlock
}

func getCornerBlocks(sideToBlockIds map[int64][]int) []int {
	// candidates maps a blockId to how many sides
	// it has that are available an odd number of times
	// The candidates with two adjacent odd-available sides
	// are hopefully likely the corners.
	candidates := map[int]int{}
	for _, blockIds := range sideToBlockIds {
		if len(blockIds)%2 == 1 {
			for _, b := range blockIds {
				candidates[b]++
			}
		}
	}
	type kv struct {
		Key   int
		Value int
	}
	var kvSlice []kv
	for k, v := range candidates {
		kvSlice = append(kvSlice, kv{k, v})
	}
	sort.Slice(kvSlice, func(i, j int) bool {
		return kvSlice[i].Value > kvSlice[j].Value
	})
	corners := []int{}
	for _, kv := range kvSlice {
		if len(corners) < 4 {
			corners = append(corners, kv.Key)
		}
	}
	return corners
}

// Looking at the sideToBlockId map we created previously,
// we see that the search is easy; sides always match
// uniquely, so we do never have to backtrack.
func solvePuzzle(upperLeft int, blockMap map[int]block) [][]int {
	return [][]int{}
}

// Call this to find the neighboring tile of tile with blockId, along side side.
func findNeighborBlock(blockId int, side int64) int {
	twoNeighbors := sideToBlockIds[canonicalSide(side)]
	var matchingTileID int
	if twoNeighbors[0] == blockId {
		matchingTileID = twoNeighbors[1]
	} else {
		matchingTileID = twoNeighbors[0]
	}
	return matchingTileID
}

// Find the tile that fits below the one with blockID
// and rotate/flip it accordingly, assuming it is in the left column
func matchBelow(blockId int) int {
	neighbor := blockMap[blockId]
	neighborBottomSide := neighbor.sides[2]
	// find the matching side that should become top now
	rotationsNeeded := 4
	matchingTileID := findNeighborBlock(blockId, neighborBottomSide)
	matchingTile := blockMap[matchingTileID]
	for _, side := range matchingTile.sides {
		if canonicalSide(side) == canonicalSide(neighborBottomSide) {
			break
		}
		rotationsNeeded--
	}
	// do the necessary rotation
	matchingTile.rotate(rotationsNeeded)
	// flip correctly, i.e. the open side should be on the left
	maybeLeftSide := matchingTile.sides[3]
	if len(sideToBlockIds[canonicalSide(maybeLeftSide)])%2 != 1 {
		// Right side should be on the left
		matchingTile.flipLR()
	}
	return matchingTileID
}

// Like matchBelow, but on the right
func matchOnRight(blockId int) int {
	neighbor := blockMap[blockId]
	neighborRightSide := neighbor.sides[1]
	// find the matching side that should become top now
	rotationsNeeded := 7
	matchingTileID := findNeighborBlock(blockId, neighborRightSide)
	matchingTile := blockMap[matchingTileID]
	for _, side := range matchingTile.sides {
		if canonicalSide(side) == canonicalSide(neighborRightSide) {
			break
		}
		rotationsNeeded--
	}
	matchingTile.rotate(rotationsNeeded)
	// flip correctly, i.e. the top side should either be open or matching
	leftSide := matchingTile.sides[3]
	if leftSide == neighbor.sides[1] {
		matchingTile.flipUD()
	}
	return matchingTileID
}

type coords struct {
	x int
	y int
}

func findSeaMonsters(fullView []string, seamonster []string) int {
	result := 0
	for row := 0; row < len(fullView)-len(seamonster)+1; row++ {
	NotAMonster:
		for col := 0; col < len(fullView[0])-len(seamonster[0])+1; col++ {
			for dr := 0; dr < len(seamonster); dr++ {
				for dc := 0; dc < len(seamonster[0]); dc++ {
					if seamonster[dr][dc] == byte('#') {
						if fullView[row+dr][col+dc] != byte('#') {
							continue NotAMonster
						}
					}
				}
			}
			result++
		}
	}
	return result
}

func main() {
	answerA := 0
	var fullView []string
	numSeamonsters := 0
	// Correct constellation always occurs but is caused randomly
	for numSeamonsters == 0 {
		lines := fileutil.ReadLines("input")
		blocksWithHeaders := strings.Split(strings.Join(lines, "\n"), "\n\n")
		blockIds = []int{}
		blockMap = map[int]*block{}
		for _, blockWithHeader := range blocksWithHeaders {
			headerBlockSlice := strings.Split(blockWithHeader, ":")
			header := headerBlockSlice[0]
			fullBlock := strings.Split(headerBlockSlice[1], "\n")[1:]
			sideLen = len(fullBlock[0])
			headerNum, _ := strconv.Atoi(header[5:])
			block := createBlock(headerNum, fullBlock)
			blockMap[headerNum] = block
			blockIds = append(blockIds, headerNum)
		}
		sideToBlockIds = createSideToBlockMap(blockMap)
		cBlocks := getCornerBlocks(sideToBlockIds)
		answerA = 1
		for _, cb := range cBlocks {
			answerA *= cb
		}
		topLeft := cBlocks[0] // pick arbitrary corner as top left
		// rotate topLeft so its unmatchable sides are top and left
		openSideIdxs, openSideIds := []int{}, []int64{}
		for idx, side := range blockMap[topLeft].sides {
			blockIds := sideToBlockIds[canonicalSide(side)]
			if len(blockIds)%2 == 1 {
				openSideIdxs = append(openSideIdxs, idx)
				openSideIds = append(openSideIds, side)
			}
		}
		if !((openSideIdxs[0] == 0 && openSideIdxs[1] == 3) || (openSideIdxs[0] == 3 && openSideIdxs[1] == 0)) {
			blockMap[topLeft].rotate(4 - mathutil.MaxInt(openSideIdxs[0], openSideIdxs[1]))
		}
		// topLeft is now the blockId of our correctly rotated starting tile, and is not flipped.
		solutionDim := int(math.Sqrt(float64(len(blockIds))))
		solution := make([][]int, solutionDim)
		for row := 0; row < solutionDim; row++ {
			solution[row] = make([]int, solutionDim)
		}
		for row := 0; row < solutionDim; row++ {
			// Place the first tile of the row based on the tile above
			if row == 0 {
				solution[0][0] = topLeft
			} else {
				solution[row][0] = matchBelow(solution[row-1][0])
			}
			for col := 1; col < solutionDim; col++ {
				solution[row][col] = matchOnRight(solution[row][col-1])
			}
		}
		fullViewWithGaps := make([]string, solutionDim*(sideLen))
		for rowIdx, row := range solution {
			for _, blockId := range row {
				// Uncomment to see individual blocks
				// fmt.Println("Block " + strconv.Itoa(rowIdx) + ", " + strconv.Itoa(colIdx) + ":")
				for blockRowIdx, rowStr := range blockMap[blockId].fullBlockStrings() {
					if blockRowIdx != 0 && blockRowIdx != sideLen-1 {
						fullViewRow := rowIdx*sideLen + blockRowIdx
						fullViewWithGaps[fullViewRow] += rowStr[1 : sideLen-1]
					}
				}
			}
		}
		fullView = []string{}
		for _, row := range fullViewWithGaps {
			if len(row) != 0 {
				fullView = append(fullView, row)
				// Uncomment to check the full view
				// fmt.Println(row)
			}
		}
		seamonsterChars := []string{
			"                  # ",
			"#    ##    ##    ###",
			" #  #  #  #  #  #   ",
		}
		// Some optimization in case I ever get really bored
		// seamonster := []coords{}
		// for rowIdx, row := range seamonsterChars {
		// 	for colIdx, col := range row {
		// 		if col == rune('#') {
		// 			seamonster = append(seamonster, coords{x: colIdx, y: rowIdx})
		// 		}
		// 	}
		// }
		numSeamonsters = findSeaMonsters(fullView, seamonsterChars)
	}
	fmt.Printf("Answer to Part 1: %d\n", answerA)
	fmt.Printf("%s%d\n", "Seamonsters found: ", numSeamonsters)
	numWaves := 0
	for row := 0; row < len(fullView); row++ {
		numWaves += strings.Count(fullView[row], "#")
	}
	// hardcoded that seamonsters are 15 waves big
	fmt.Printf("%s%d\n", "Waves without seamonster: ", numWaves-numSeamonsters*15)
}
