package main

import (
	"fmt"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
	"gitlab.com/DiningPhilosopher/go/sliceutil"
)

var ingr2allg map[string][]string
var allg2ingr map[string][]string

func addOption(newIngr string, newAllg string) {
	if sliceutil.FindString(ingr2allg[newIngr], newAllg) == -1 {
		ingr2allg[newIngr] = append(ingr2allg[newIngr], newAllg)
	}
	if sliceutil.FindString(allg2ingr[newAllg], newIngr) == -1 {
		allg2ingr[newAllg] = append(allg2ingr[newAllg], newIngr)
	}
}

func removeOption(ingr string, allg string) {
	allgIdx := sliceutil.FindString(ingr2allg[ingr], allg)
	if allgIdx == -1 {
		fmt.Println("Warning: trying to remove allergen for unused option " + ingr + " - " + allg)
	} else {
		ingr2allg[ingr] = sliceutil.RemoveStringAtIdx(ingr2allg[ingr], allgIdx)
	}
	ingrIdx := sliceutil.FindString(allg2ingr[allg], ingr)
	if ingrIdx == -1 {
		fmt.Println("Warning: trying to remove ingredient for unused option " + ingr + " - " + allg)
	} else {
		allg2ingr[allg] = sliceutil.RemoveStringAtIdx(allg2ingr[allg], ingrIdx)
	}
}

func main() {
	lines := fileutil.ReadLines("input")
	ingrs := []string{}
	allgs := []string{}
	inputs := [][][]string{}
	for _, l := range lines {
		ingrAndAllg := strings.Split(l, " (contains ")
		newIngrs := strings.Split(ingrAndAllg[0], " ")
		for _, ingr := range newIngrs {
			if sliceutil.FindString(ingrs, ingr) == -1 {
				ingrs = append(ingrs, ingr)
			}
		}
		newAllgsWithTrail := strings.Split(ingrAndAllg[1], " ")
		newAllgs := []string{}
		for _, item := range newAllgsWithTrail {
			allg := item[0 : len(item)-1]
			newAllgs = append(newAllgs, allg)
			if sliceutil.FindString(allgs, allg) == -1 {
				allgs = append(allgs, allg)
			}
		}
		newInput := [][]string{newIngrs, newAllgs}
		inputs = append(inputs, newInput)

	}
	ingr2allg = make(map[string][]string, len(ingrs))
	allg2ingr = make(map[string][]string, len(allgs))
	allgInitialized := map[string]bool{}
	for _, ingr := range ingrs {
		ingr2allg[ingr] = []string{}
	}
	for _, allg := range allgs {
		allgInitialized[allg] = false
		allg2ingr[allg] = []string{}
	}
	// Iterate over input again
	for _, input := range inputs {
		ingrs := input[0]
		allgs := input[1]
		for _, allg := range allgs {
			if !allgInitialized[allg] {
				for _, ingr := range ingrs {
					addOption(ingr, allg)
				}
				allgInitialized[allg] = true
			} else {
				toRemove := []string{}
				for _, oldIngr := range allg2ingr[allg] {
					if sliceutil.FindString(ingrs, oldIngr) == -1 {
						toRemove = append(toRemove, oldIngr)
					}
				}
				for _, ingr := range toRemove {
					removeOption(ingr, allg)
				}
			}
		}
	}
	impossibleIngrs := []string{}
	for ingr, allg := range ingr2allg {
		if len(allg) == 0 {
			impossibleIngrs = append(impossibleIngrs, ingr)
		}
	}
	total := 0
	for _, imp := range impossibleIngrs {
		for _, inp := range inputs {
			if sliceutil.FindString(inp[0], imp) != -1 {
				total++
			}
		}
		delete(ingr2allg, imp)
	}
	fmt.Println(total)
	for k, v := range ingr2allg {
		fmt.Println(k)
		fmt.Println(v)
	}
	// The rest is left as a manual exercise to the reader.
	// Answer: bjpkhx,nsnqf,snhph,zmfqpn,qrbnjtj,dbhfd,thn,sthnsg
}
