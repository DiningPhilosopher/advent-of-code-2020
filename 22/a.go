package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

func main() {
	lines := fileutil.ReadLines("input")
	decks := [][]int{[]int{}, []int{}}
	// populate decks
	playerNum := -1
	for _, l := range lines {
		if strings.HasPrefix(l, "Player") {
			playerNum++
		} else if len(l) > 0 {
			val, _ := strconv.Atoi(l)
			decks[playerNum] = append(decks[playerNum], val)
		}
	}
	for len(decks[0]) > 0 && len(decks[1]) > 0 {
		c0, c1 := decks[0][0], decks[1][0]
		if c0 > c1 {
			decks[0] = append(decks[0][1:len(decks[0])], c0, c1)
			decks[1] = decks[1][1:len(decks[1])]
		} else {
			decks[1] = append(decks[1][1:len(decks[1])], c1, c0)
			decks[0] = decks[0][1:len(decks[0])]
		}
	}
	fmt.Println(decks)
	winDeck := decks[0]
	if len(winDeck) == 0 {
		winDeck = decks[1]
	}
	total := 0
	for idx, w := range winDeck {
		total += (len(winDeck) - idx) * w
	}
	fmt.Println("Part 1:", total)
}
