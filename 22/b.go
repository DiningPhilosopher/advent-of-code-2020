package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

const DEBUG = false

type tree struct {
	terminating bool
	sub         map[int]*tree
}

func newTree(terminating bool) *tree {
	return &tree{terminating: terminating, sub: map[int]*tree{}}
}

func (t *tree) contains(config [][]int) bool {
	currentTree := t
	for playerIdx, playerDeck := range config {
		if subTree, present := currentTree.sub[-playerIdx]; !present {
			return false
		} else {
			currentTree = subTree
		}
		for _, c := range playerDeck {
			if subTree, present := currentTree.sub[c]; present {
				currentTree = subTree
			} else {
				return false
			}
		}
	}
	return currentTree.terminating
}

func (t *tree) addConfig(decks [][]int) {
	currentTree := t
	for playerIdx := 0; playerIdx < 2; playerIdx++ {
		if _, present := currentTree.sub[-playerIdx]; !present {
			currentTree.sub[-playerIdx] = newTree(false)
		}
		currentTree = currentTree.sub[-playerIdx]
		for cIdx, c := range decks[playerIdx] {
			if _, present := currentTree.sub[c]; !present {
				terminating := false
				if playerIdx == 1 && cIdx == len(decks[1])-1 {
					terminating = true
				}
				currentTree.sub[c] = newTree(terminating)
			}
			currentTree = currentTree.sub[c]
		}
	}
}

func (t *tree) printTree(prefix string) {
	if t.terminating {
		fmt.Printf("@\n")
	}
	for num, sub := range t.sub {
		fmt.Printf("%s%d\n", prefix, num)
		sub.printTree(prefix + "*")
	}
}

var globalGameNumber int

func playGame(decks [][]int) (int, []int) {
	gameNumber := globalGameNumber
	globalGameNumber++
	configTree := newTree(false)
	for len(decks[0]) > 0 && len(decks[1]) > 0 {
		if DEBUG {
			fmt.Printf("Player 0's deck: %v\nPlayer 1's deck: %v\n", decks[0], decks[1])
		}
		if present := configTree.contains(decks); present {
			if DEBUG {
				fmt.Println("Player 0 wins by repetition!\n")
			}
			return 0, decks[0]
		}
		configTree.addConfig(decks)
		c0, c1 := decks[0][0], decks[1][0]
		winner := 0
		if c0 < len(decks[0]) && c1 < len(decks[1]) {
			if DEBUG {
				fmt.Println("Playing a sub-game to determine the winner...\n")
			}
			sd0, sd1 := decks[0][1:c0+1], decks[1][1:c1+1]
			subDeck0 := make([]int, len(sd0))
			copy(subDeck0, sd0)
			subDeck1 := make([]int, len(sd1))
			copy(subDeck1, sd1)
			subDecks := [][]int{subDeck0, subDeck1}
			winner, _ = playGame(subDecks)
			if DEBUG {
				fmt.Printf("...anyway, back to game %d\n", gameNumber)
			}
		} else {
			if c0 > c1 {
				winner = 0
			} else {
				winner = 1
			}
		}
		if winner == 0 {
			decks[0] = append(decks[0][1:len(decks[0])], c0, c1)
			decks[1] = decks[1][1:len(decks[1])]
		} else {
			decks[1] = append(decks[1][1:len(decks[1])], c1, c0)
			decks[0] = decks[0][1:len(decks[0])]
		}
	}
	winDeck := decks[0]
	winner := 0
	if len(winDeck) == 0 {
		winner = 1
		winDeck = decks[1]
	}
	if DEBUG {
		fmt.Printf("Player %d wins by having all cards!\n\n", winner)
	}
	return winner, winDeck
}

func main() {
	globalGameNumber = 1
	lines := fileutil.ReadLines("input")
	decks := [][]int{[]int{}, []int{}}
	// populate decks
	playerNum := -1
	for _, l := range lines {
		if strings.HasPrefix(l, "Player") {
			playerNum++
		} else if len(l) > 0 {
			val, _ := strconv.Atoi(l)
			decks[playerNum] = append(decks[playerNum], val)
		}
	}
	winPlayer, winDeck := playGame(decks)
	fmt.Printf("Player %v wins with %v\n", winPlayer, winDeck)
	total := 0
	for idx, w := range winDeck {
		total += (len(winDeck) - idx) * w
	}
	fmt.Println("Part 2", total)
}
