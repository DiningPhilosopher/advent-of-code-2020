package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/DiningPhilosopher/go/sliceutil"
)

func round(cups []int) []int {
	current := cups[0]
	three := cups[1:4]
	threeOut := append([]int{current}, cups[4:]...)
	lower := current - 1
	destIdx := 0
FindDestination:
	for {
		for newIdx, cup := range threeOut {
			if cup == lower {
				destIdx = newIdx
				break FindDestination
			}
		}
		lower--
		if lower < 1 {
			lower = 9
		}
	}
	before := make([]int, destIdx)
	after := make([]int, len(threeOut)-destIdx-1)
	copy(before, threeOut[1:destIdx+1])
	copy(after, threeOut[destIdx+1:])
	result := append(before, three...)
	result = append(result, after...)
	result = append(result, threeOut[0])
	return result
}
func main() {
	filename := "input"
	file, err := os.Open(filename)
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Println("Error closing file.")
			os.Exit(1)
		}
	}(file)
	if err != nil {
		fmt.Println("Problem reading file " + filename)
		os.Exit(1)
	}
	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := scanner.Text()
	cups := []int{}
	for _, char := range input {
		num, _ := strconv.Atoi(string(char))
		cups = append(cups, num)
	}
	totalRounds := 10
	for roundNum := 0; roundNum < totalRounds; roundNum++ {
		cups = round(cups)
		fmt.Println(cups)
	}
	oneIdx := sliceutil.FindInt(cups, 1)
	for idx := oneIdx + 1; idx < len(cups); idx++ {
		fmt.Print(cups[idx])
	}
	for idx := 0; idx < oneIdx; idx++ {
		fmt.Print(cups[idx])
	}
	fmt.Println()
}
