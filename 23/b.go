package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type numCell struct {
	val   int
	lower *numCell
	next  *numCell
}

var oneCell *numCell

func round(currentCell *numCell) *numCell {
	dest := currentCell.lower
	three1, three2, three3 := currentCell.next, currentCell.next.next, currentCell.next.next.next
	for dest.val == three1.val || dest.val == three2.val || dest.val == three3.val {
		dest = dest.lower
	}
	// destination determined, splice in the three after it
	oldDestNext := dest.next
	dest.next = three1
	oldThree3Next := three3.next
	three3.next = oldDestNext
	currentCell.next = oldThree3Next
	return currentCell.next
}

func createRing(input string, ringSize int) *numCell {
	var newCell, prevCell, startCell *numCell
	numToCells := map[int]*numCell{}
	for num := 1; num <= ringSize; num++ {
		newCell = &numCell{
			val: num,
		}
		if num == 1 {
			oneCell = newCell
		}
		if prevCell != nil {
			newCell.lower = prevCell
			if prevCell.val > len(input) {
				prevCell.next = newCell
			}
		}
		// make finding the cells for our input easier
		if num <= len(input)+1 {
			numToCells[num] = newCell
		}
		prevCell = newCell
	}
	// Set lower-pointer to the last cell
	oneCell.lower = newCell
	// Set all the next-pointers in our input
	num1, num2 := 0, 0
	for cellIdx, char := range input {
		num2, _ = strconv.Atoi(string(char))
		if cellIdx == 0 {
			// set next-pointer of last cell
			startCell = numToCells[num2]
			numToCells[1].lower.next = startCell
		} else {
			numToCells[num1].next = numToCells[num2]
		}
		num1 = num2
	}
	// set next pointer of last input cell
	numToCells[num2].next = numToCells[len(input)+1]
	return startCell
}

func printRing(startCell *numCell) {
	cell := startCell
	for {
		fmt.Printf("%2d ", cell.val)
		cell = cell.next
		if cell == startCell {
			break
		}
	}
	fmt.Println()
}

func main() {
	filename := "input"
	file, err := os.Open(filename)
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Println("Error closing file.")
			os.Exit(1)
		}
	}(file)
	if err != nil {
		fmt.Println("Problem reading file " + filename)
		os.Exit(1)
	}
	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := scanner.Text()
	currentCell := createRing(input, 1000000)
	// Entire ring initialized, let's play!
	totalRounds := 10000000
	for roundNum := 0; roundNum < totalRounds; roundNum++ {
		currentCell = round(currentCell)
	}
	num1 := oneCell.next
	num2 := oneCell.next.next
	fmt.Println(num1.val * num2.val)
}
