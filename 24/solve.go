package main

import (
	"fmt"
	"math"

	"github.com/golang/geo/r2"
	"gitlab.com/DiningPhilosopher/go/fileutil"
)

const (
	_ = iota
	NE
	E
	SE
	SW
	W
	NW
)

var (
	pointNE = r2.Point{Y: 1}
	pointE  = r2.Point{X: 1}
	pointSE = r2.Point{X: 1, Y: -1}
	pointSW = r2.Point{Y: -1}
	pointW  = r2.Point{X: -1}
	pointNW = r2.Point{X: -1, Y: 1}
)

func createTokens(in string) []int {
	idx := 0
	tokens := []int{}
	t := -1
	for idx < len(in) {
		if in[idx] == byte('e') {
			t = E
		} else if in[idx] == byte('w') {
			t = W
		} else if in[idx] == byte('n') {
			idx++
			if in[idx] == byte('e') {
				t = NE
			} else if in[idx] == byte('w') {
				t = NW
			}
		} else if in[idx] == byte('s') {
			idx++
			if in[idx] == byte('e') {
				t = SE
			} else if in[idx] == byte('w') {
				t = SW
			}
		}
		tokens = append(tokens, t)
		idx++
	}
	return tokens
}

func pointFromTokens(tokens []int) r2.Point {
	x, y := 0, 0
	for _, t := range tokens {
		if t == W {
			x--
		} else if t == E {
			x++
		} else if t == NE {
			y++
		} else if t == SE {
			x++
			y--
		} else if t == SW {
			y--
		} else if t == NW {
			x--
			y++
		}
	}
	return r2.Point{X: float64(x), Y: float64(y)}
}

func getNeighbors(tile r2.Point) []r2.Point {
	neighbors := []r2.Point{}
	allDirs := []r2.Point{pointNE, pointE, pointSE, pointSW, pointW, pointNW}
	for _, dir := range allDirs {
		neighbor := tile.Add(dir)
		neighbor.X = math.Round(neighbor.X)
		neighbor.Y = math.Round(neighbor.Y)
		neighbors = append(neighbors, neighbor)
	}
	return neighbors
}

func nextDay(tileMap map[r2.Point]bool) map[r2.Point]bool {
	nextMap := map[r2.Point]bool{}
	// First add all whites next to blacks, these potentially turn black
	whiteTiles := map[r2.Point]bool{}
	for tile, _ := range tileMap {
		for _, neigh := range getNeighbors(tile) {
			if !tileMap[neigh] {
				whiteTiles[neigh] = false // black = true, white = false
			}
		}
	}
	for white, isBlack := range whiteTiles {
		tileMap[white] = isBlack
	}
	for tile, isBlack := range tileMap {
		neighbors := getNeighbors(tile)
		blackNeighbors := 0
		for _, neighbor := range neighbors {
			if tileMap[neighbor] {
				blackNeighbors++
			}
		}
		if isBlack && (blackNeighbors == 1 || blackNeighbors == 2) {
			// tile stays black, otherwise make white, which is implicit (false)
			nextMap[tile] = true
		}
		if !isBlack && blackNeighbors == 2 {
			nextMap[tile] = true
		}
	}
	return nextMap
}

func countBlack(tileMap map[r2.Point]bool) int {
	total := 0
	for _, v := range tileMap {
		if v {
			total++
		}
	}
	return total
}

func printMap(tileMap map[r2.Point]bool) {
	for tile, black := range tileMap {
		if black {
			fmt.Printf("(%d, %d) ", int(tile.X), int(tile.Y))
		}
	}
	fmt.Println()
}
func main() {
	lines := fileutil.ReadLines("input")
	tileMap := map[r2.Point]bool{}
	var p r2.Point
	for _, l := range lines {
		tokens := createTokens(l)
		p = pointFromTokens(tokens)
		tileMap[p] = !tileMap[p] // values are implicitly false
	}
	fmt.Println("Part 1:", countBlack(tileMap))
	for day := 0; day < 100; day++ {
		tileMap = nextDay(tileMap)
	}
	fmt.Println("Part 2:", countBlack(tileMap))
}
