package main

import (
	"fmt"
	"strconv"

	"gitlab.com/DiningPhilosopher/go/fileutil"
)

const MODULUS = 20201227

func outToLoopNum(out int64) int64 {
	loops := int64(0)
	num := int64(1)
	for num != out {
		loops++
		num = (num * 7) % MODULUS
	}
	return loops
}

func transform(subj int64, loops int64) int64 {
	out := int64(1)
	for l := int64(0); l < loops; l++ {
		out = (out * subj) % MODULUS
	}
	return out
}

func main() {
	lines := fileutil.ReadLines("input")
	out1, _ := strconv.ParseInt(lines[0], 10, 64)
	out2, _ := strconv.ParseInt(lines[1], 10, 64)
	loop2 := outToLoopNum(out2)
	fmt.Println(transform(out1, loop2))
}
