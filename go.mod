module gitlab.com/DiningPhilosopher/advent-of-code-2020

go 1.15

replace gitlab.com/DiningPhilosopher/go/fileutil => ../../Go/fileutil

replace gitlab.com/DiningPhilosopher/go/stringutil => ../../Go/stringutil

replace gitlab.com/DiningPhilosopher/go/mathutil => ../../Go/mathutil

replace gitlab.com/DiningPhilosopher/go/sliceutil => ../../Go/sliceutil

require (
	github.com/golang/geo v0.0.0-20200730024412-e86565bf3f35 // indirect
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
	github.com/stretchr/stew v0.0.0-20130812190256-80ef0842b48b
	github.com/stretchr/testify v1.6.1 // indirect
	gitlab.com/DiningPhilosopher/go/fileutil v0.0.0-00010101000000-000000000000
	gitlab.com/DiningPhilosopher/go/mathutil v0.0.0-00010101000000-000000000000 // indirect
	gitlab.com/DiningPhilosopher/go/sliceutil v0.0.0-00010101000000-000000000000
	gitlab.com/DiningPhilosopher/go/stringutil v0.0.0-00010101000000-000000000000 // indirect
	golang.org/x/tools v0.0.0-20201215192005-fa10ef0b8743 // indirect
)
